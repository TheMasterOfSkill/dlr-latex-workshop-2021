# DLR LaTeX Workshop 2021

This is the repository of the official 2021 DLR LaTeX workshop for freshmen.

Go to the Gitlab Wiki for an installation manual.

## Structure

`./.vscode/`: VS Codium workspace settings

`./docs/`: All .tex files

`./img/`: All image files

`./lib/`: All .bib files

`./out/`: LaTeX build artifacts

`./snippets/`: Supplementary material (optional)
