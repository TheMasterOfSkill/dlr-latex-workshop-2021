%!TEX root = ./document.tex
\chapter{Mathematical Environments}
\label{ch:math}

This chapter shows how to use the mathematical environments in \LaTeX~in order to format formulas, equations and more. 
\Cref{ch:math.inline,ch:math.equation} present the inline mode and the equation environment.
In subsequence, \cref{ch:math.freqCommands} gives an overview of the most frequently used commands and symbols in mathematical environments.
Eventually, \cref{ch:math.siunitx} introduces functionality to correctly format numbers and units.


\section{Inline Mode}
\label{ch:math.inline}

The \textit{inline mode} is, as the name suggests, to embed mathematical expressions in the same line as the surrounding text.
This is useful if we want to refer to single expressions directly in the text without making a separate paragraph.
The inline mode is activated by the symbol \verb!$!, which acts both as an opening and closing tag.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        Our parameters are $a=1, b=2, c=3$.
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        Our parameters are $a=1, b=2, c=3$.
    \end{indentBuilt}
\end{example}


\section{Equation Environment}
\label{ch:math.equation}

In general, it is useful to be able to refer to previously stated formulas or equations. 
For this purpose, \LaTeX~provides the \textit{equation environment}, which is opened and closed by the tags \verb!\begin{equation}! and \verb!\end{equation}!, respectively.
Inside these tags, we can use all math-related commands as in the inline mode.
In order to establish the possibility for cross-references, we have to add a label to the equation.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \begin{equation}
            a^2 + b^2 = c^2
            \label{eq:pythagoras}
        \end{equation}
    \end{indentVerbatim}

    \noindent leads to

    \begin{equation}
        a^2 + b^2 = c^2,
        \label{eq:pythagoras}
    \end{equation}

    \noindent which can subsequently be referred to as shown in \cref{ch:basic.references} (resulting in \enquote{\cref{eq:pythagoras}}).
\end{example}


\section{Frequently Used Commands and Symbols}
\label{ch:math.freqCommands}

When we operate in a mathematical environment, either the inline mode, the equation environment or any other, the syntax is always the same and quite intuitive.
Actually, many symbols that are on the keyboard are rendered one-to-one, e.g., the plus or minus signs.

Furthermore, the superscript and subscript notation is implemented using the circumflex \verb!^! and the underscore \verb!_!, respectively.
Here, we only have to be careful when an expression with more than a single symbol is to be rendered in superscript or subscript.
In that case, we have to embed this expression in a block that is opened and closed by curly brackets.
Last, when both subscript and superscript notation is to be used, the order does not matter.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        $x^1_0 - y^{12}_{34} = x_0^1 - y_{34}^{12}$
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        $x^1_0 - y^{12}_{34} = x_0^1 - y_{34}^{12}$.
    \end{indentBuilt}
\end{example}
\vspace{1cm}

A common mistake is to use the asterisk \verb!*! when one actually wants to write a multiplication sign, since it is also rendered as an asterisk and actually denotes the convolution operator.
Instead, we either have to use the command \verb!\cdot! or simply omit it, which is often better style.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        $xy = x \cdot y \neq x * y$
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        $xy = x \cdot y \neq x * y$.
    \end{indentBuilt}
\end{example}
\vspace{1cm}

Moreover, we can format fractions by simply writing a slash between nominator and denominator, but in most cases, it is better style to use the command \verb!\frac{}{}!, where the first argument represents the nominator and the second one the denominator.
Depending on the available and required space, \LaTeX~automatically scales the size of the fraction.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        $1/2 = \frac{1}{2}$
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        $1/2 = \frac{1}{2}$.
    \end{indentBuilt}
\end{example}
\vspace{1cm}

When parentheses are used to encapsulate a contiguous term, they are always in default scale.
To scale them automatically, depending on the size of the expression being enclosed, we have to use the prefixes \verb!\left! and \verb!\right!.
This applies analogously to other types of brackets.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        $2(\frac{1}{4} + \frac{1}{4}) = 2\left(\frac{1}{4} + \frac{1}{4}\right) = 1$
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        $2(\frac{1}{4} + \frac{1}{4}) = 2\left(\frac{1}{4} + \frac{1}{4}\right) = 1$.
    \end{indentBuilt}
\end{example}
\vspace{1cm}

Last, \cref{tab:freqMathSymbols} lists the most frequently used mathematical symbols as well as the corresponding command in \LaTeX.
For commands and symbols beyond these, \url{https://en.wikibooks.org/wiki/LaTeX/Mathematics} is a good port for call.
For some commands and symbols, it is necessary to load the package \textit{amsmath} \autocite{latexprojectteamAmsmathPackage2020}, which is generally recommended.
It also provides further mathematical environments, e.g., to split and align multi-line equations.

\begin{table}[!ht]
    \centering
    \caption[Frequently used mathematical symbols]{Frequently used mathematical symbols. The left column contains the names of the symbols, the middle column shows corresponding example code in \LaTeX~and the right column shows the rendered text.}\label{tab:freqMathSymbols}

    \begin{tabular}{lcc} 
        \toprule
        Name & \LaTeX & Rendered\\
        \midrule
        Inequality & \verb!0 \neq 1! & $0 \neq 1$ \\
        Less than or equal to & \verb!0 \leq 1! & $0 \leq 1$ \\
        Greater than or equal to & \verb!1 \geq 0! & $1 \geq 0$ \\
        Element of & \verb!x \in X! & $x \in X$ \\

        Infinity & \verb!\infty! & $\infty$ \\
        Pi & \verb!\pi! & $\pi$ \\
        
        Square root & \verb!\sqrt{x}! & $\sqrt{x}$ \\
        n-th root & \verb!\sqrt[n]{x}! & $\sqrt[n]{x}$ \\
        Sine & \verb!\sin{x}! & $\sin{x}$ \\
        Cosine & \verb!\cos{x}! & $\cos{x}$ \\
        Tangent & \verb!\tan{x}! & $\tan{x}$ \\
        Exponential function & \verb!\exp{x}! & $\exp{x}$ \\

        Summation & \verb!\sum_{i=1}^n{i}! & $\sum_{i=1}^n{i}$ \\
        Integration & \verb!\int_0^\infty{f(x)}\,\mathrm{d}x! & $\int_0^\infty{f(x)}\,\mathrm{d}x$ \\
        Derivative & \verb!\frac{\partial f(x, y)}{\partial x}! & $\frac{\partial f(x, y)}{\partial x}$ \\
        Limes & \verb!\lim\limits_{x \to \infty}{f(x)}! & $\lim\limits_{x \to \infty}{f(x)}$ \\
        \bottomrule
    \end{tabular}
\end{table}


\section{Numbers and Units}
\label{ch:math.siunitx}

Last but not least, we go through the most important functions of the package \textit{siunitx} \autocite{wrightSiunitxComprehensiveSI2021}.
It can be used to simplify the correct formatting of (complex) numbers and units.
Here, all provided commands can be used both inside and outside mathematical environments.

First of all, the command \verb!\num{}! formats real numbers correctly, including the digit grouping for large numbers and the rendering of the scientific notation.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        $\num{2000000} = \num{2e6}$
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        $\num{2000000} = \num{2e6}$.
    \end{indentBuilt}
\end{example}
\vspace{1cm}

Next, the command \verb!\unit{}! allows easy handling of units. 
There are two different modes of usage:
In the first one, we simply insert the units in their abbreviated forms as strings, where a product is denoted by \verb!.! and a quotient and a power are written as usual by \verb!/! and \verb!^!, respectively.
In the second one, we can use pre-defined \LaTeX~macros to express both units and mathematical operators.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        $\unit{kg.m.s^{-2}} = \unit{\kilo\gram\metre\per\square\second}$
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        $\unit{kg.m.s^{-2}} = \unit{\kilo\gram\metre\per\square\second}$.
    \end{indentBuilt}
\end{example}
\vspace{1cm}

The combination of both \verb!\num{}! and \verb!\unit{}! is represented by the command \\\verb!\qty{}{}!, whose first argument is the real number and whose second argument is the unit.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        $\qty{2000000}{kg.m.s^{-2}} = \qty{2e6}{\kilo\gram\metre\per\square\second}$
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        $\qty{2000000}{kg.m.s^{-2}} = \qty{2e6}{\kilo\gram\metre\per\square\second}$.
    \end{indentBuilt}
\end{example}
\vspace{1cm}

Angles are a little special case, since they are not formatted using \verb!\qty{}{}!, but with the command \verb!\ang{}!.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \begin{equation}
            \frac{\alpha}{\ang{360}} = \frac{x}{2\pi}
            \label{eq:AngRadConversion}
        \end{equation}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \begin{equation}
            \frac{\alpha}{\ang{360}} = \frac{x}{2\pi}.
            \label{eq:AngRadConversion}
        \end{equation}
    \end{indentBuilt}
\end{example}
\vspace{1cm}

We write complex numbers and quantities by the commands \verb!\complexnum{}! and \verb!\complexqty{}{}!.
Here, the corresponding argument must be in standard form $a + bi$.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        $\left(\complexnum{0.00001 + 100000i}\right)\complexqty{100000 + 0.00001i}{\volt}$
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        $\left(\complexnum{0.00001 + 100000i}\right)\complexqty{100000 + 0.00001i}{\volt}$.
    \end{indentBuilt}
\end{example}
\vspace{1cm}

Last, there are also further commands which allow, e.g., the formatting of products or ranges between numbers or quantities.
However, these are beyond the scope of this script.
If such functionality is needed, it is recommended to read the documentation of the siuintx package \autocite{wrightSiunitxComprehensiveSI2021}.