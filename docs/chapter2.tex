%!TEX root = ./document.tex
\chapter{Basic Functionality and Packages}
\label{ch:basic}

This chapter gives an overview of the most important basic functionalities that come either as standard tools of \LaTeX~or have to be loaded via additional packages.
First, \cref{ch:basic.geometry} presents the geometry package, being followed by \cref{ch:basic.lists}, which gives an explanation on formatting lists.
\Cref{ch:basic.fonts} shows how to use other fronts than the default one.
Subsequently, in \cref{ch:basic.fontStyles,ch:basic.fontSizes}, we go through the syntax of different font styles and font sizes in \LaTeX, respectively.
\Cref{ch:basic.xcolor} then explains the usage of the package xcolor to colorize different elements in a document, and \cref{ch:basic.titlepage} presents the environment to set up a title page.
Next, the use of the acronyms package is expounded in \cref{ch:basic.acronyms}, and \cref{ch:basic.references} shows how to make cross-references.
In \cref{ch:basic.babel}, the babel package is introduced as a language checker.
Last but not least, \cref{ch:basic.biblatex} explains how cite and manage literature, and \cref{ch:basic.footnotes} shows the syntax of defining footnotes.


\section{Geometry}
\label{ch:basic.geometry}

One of the most basic, but nevertheless highly relevant packages is \textit{geometry} \autocite{umekiGeometryPackage2020}.
It provides an interface to adjust page dimensions, e.g., the margins.
A very common configuration is \qty{2.5}{\centi\metre} horizontal (left/right) margins, \qty{3}{\centi\metre} vertical (top/bottom) margins and another \qty{1}{\centi\metre} for the binding offset.\footnote{This is exactly the configuration of this document.}
With the geometry package loaded, this can be set as follows in the preamble of a \LaTeX~document:

\begin{indentVerbatimOnly}
    \geometry{top=3cm, right=2.5cm, bottom=3cm, left=2.5cm, bindingoffset=1cm}
\end{indentVerbatimOnly}


\section{Lists}
\label{ch:basic.lists}

\LaTeX~provides environments for both unordered and ordered lists that can be used without loading an additional package.
These environments are opened by the tags \verb!\begin{itemize}! and \verb!\begin{enumerate}! as well as closed by the tags \verb!\end{itemize}! and \verb!\end{enumerate}!, respectively.
Within the environments, we create a new bullet/enumeration point by the command \verb!\item!, being followed by the desired text.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \begin{itemize}
            \item 1
            \item 2
            \item 3
        \end{itemize}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \begin{itemize}
            \item 1
            \item 2
            \item 3
        \end{itemize}
    \end{indentBuilt}
\end{example}
\vspace{1cm}

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \begin{enumerate}
            \item A
            \item B
            \item C
        \end{enumerate}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \begin{enumerate}
            \item A
            \item B
            \item C
        \end{enumerate}
    \end{indentBuilt}
\end{example}
\vspace*{1cm}

Another often used list environment is \textit{description}, which is displayed
in the following example and allows highlighted terms for each item.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \begin{description}
            \item[Dog] The dog or domestic dog (Canis familiaris) is a domesticated descendant of the wolf
            \item[Dog, Chihuahua] The Chihuahua is one of the smallest breeds of
            dog. See also guinea pig.
        \end{description}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \begin{description}
            \item[Dog] The dog or domestic dog (Canis familiaris) is a domesticated descendant of the wolf
            \item[Dog, Chihuahua] The Chihuahua is one of the smallest breeds of
            dog. See also guinea pig.
        \end{description}
    \end{indentBuilt}
\end{example}

\section{Fonts}
\label{ch:basic.fonts}

With the default pdfTeX engine\footnote{Again, LuaTeX provides some additional features here.}, it is only possible to use fonts different from the (ugly) default one by loading them as packages.

A common one is the Latin Modern font.
It can be used by loading the package \textit{lmodern}.
Another one is Linux Libertine, which is included in the package \textit{libertine} \autocite{tennentLATEXSupportLinux2018}.
Here, it is recommended to load the packages \textit{newtxmath} and \textit{beramono} as well to provide suitable font styles for mathematical environments (see \cref{ch:math}) and monospace, respectively:

\begin{indentVerbatimOnly}
    \usepackage{libertine}
    \usepackage[libertine]{newtxmath}
    \usepackage[scaled=0.85]{beramono}
\end{indentVerbatimOnly}

\noindent The above configuration is used in this document as well.
In general, font-related packages should be loaded directly after inputenc, fontenc and microtype.

\section{Font Styles}
\label{ch:basic.fontStyles}

Font styles like boldface or italics can be used without any additional package as well.
\Cref{tab:fontStyles} lists the corresponding \LaTeX~commands.

\begin{table}[!ht]
    \centering
    \caption[Font styles]{Font styles. The left column contains the names of the font styles, the middle column shows corresponding example code in \LaTeX~and the right column shows the rendered text.}\label{tab:fontStyles}

    \begin{tabular}{lcc} 
        \toprule
        Name & \LaTeX & Rendered\\
        \midrule
        Normal (roman font) & \verb!\textrm{Text}! & \textrm{Text} \\
        Sans-serif & \verb!\textsf{Text}! & \textsf{Text} \\
        Monospace & \verb!\texttt{Text}! & \texttt{Text} \\
        Boldface & \verb!\textbf{Text}! & \textbf{Text} \\
        Italics & \verb!\textit{Text}! & \textit{Text} \\
        Underlined & \verb!\underline{Text}! & \underline{Text} \\
        Small capitals & \verb!\textsc{Text}! & \textsc{Text} \\
        Capitals & \verb!\uppercase{Text}! & \uppercase{Text} \\
        \bottomrule
    \end{tabular}
\end{table}


\section{Font Sizes}
\label{ch:basic.fontSizes}

\LaTeX~also provides a set of commands that activate predefined font sizes, being listed in \cref{tab:fontSizes}.
Their scope is always limited to the paragraph in which they are invoked.

\begin{table}[!ht]
    \centering
    \caption[Font sizes]{Font sizes \autocite{LaTeXFonts2021}. The left column contains the \LaTeX~command, the right column the corresponding font size in pt. All values are relative to the normal font size, which is assumed to be 12pt.}\label{tab:fontSizes}

    \begin{tabular}{lcc} 
        \toprule
        \LaTeX & Font size (pt)\\
        \midrule
        \verb!\tiny! & \num{6} \\
        \verb!\scriptsize! & \num{8} \\
        \verb!\footnotesize! & \num{10} \\
        \verb!\small! & \num{10.95} \\
        \verb!\normalsize! & \num{12} \\
        \verb!\large! & \num{14.4} \\
        \verb!\Large! & \num{17.28} \\
        \verb!\LARGE! & \num{20.74} \\
        \verb!\huge! & \num{24.88} \\
        \bottomrule
    \end{tabular}
\end{table}


\section{Coloring}
\label{ch:basic.xcolor}

The package \textit{xcolor} \autocite{kernExtendingLATEXColor2016} allows us to colorize either text or specific objects.
It extends the basic \textit{color} package \autocite{carlisleColorPackage2020} and provides additional features, e.g., to define custom colors.
Note that we have to load only xcolor, but not the basic color package.

Inside plain text, the commands \verb!\textcolor{}{}! and \verb!\colorbox{}{}! colorize the text that is passed as the second argument and its background, respectively.
In both cases, the first argument is the color.

The package provides rich sets of predefined colors, which must be stated when loading the package.
For example,

\begin{indentVerbatimOnly}
    \usepackage[dvipsnames]{xcolor}
\end{indentVerbatimOnly}

\noindent loads a set of \num{68} \ac{cmyk} colors as defined in the \texttt{dvips} driver \autocite{rokickiDvipsDVItoPostScriptTranslator2021}.
Other sets of predifined colors can be found in the documentation \autocite{kernExtendingLATEXColor2016}.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \textcolor{red}{Red text}\\
        \colorbox{green}{Green background}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \textcolor{red}{Red text}\\
        \colorbox{green}{Green background}
    \end{indentBuilt}
\end{example}
\vspace{1cm}

We can also colorize an entire environment by invoking the command \verb!\color{}! from inside.
Here, the only passed argument is the color.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \begin{itemize}
            \color{blue}
            \item A blue item
            \item Another blue item
        \end{itemize}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \begin{itemize}
            \color{blue}
            \item A blue item
            \item Another blue item
        \end{itemize}
    \end{indentBuilt}
\end{example}
\vspace{1cm}

Last, custom colors are defined by the command \verb!\definecolor{}{}{}!, where the first argument is the name of the color, the second one is the color space in which it is to be defined, and the third one is the actual definition as a comma-separated list.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \definecolor{customRuby}{rgb}{0.6,0,0.1}
        \colorbox{customRuby}{\textcolor{white}{White text on ruby colored ground}}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \definecolor{customRuby}{rgb}{0.56,0,0.11}
        \colorbox{customRuby}{\textcolor{white}{White text on ruby colored ground}}
    \end{indentBuilt}
\end{example}


\section{Title Page}
\label{ch:basic.titlepage}

A title page is best outsourced to a separate file, e.g., \texttt{titlepage.tex}.
In subsequence, it is included in the root \texttt{.tex} as shown in \cref{ch:introduction.documentText}.
We can design the title page in a special \LaTeX~environment, which is opened and closed by the tags \verb!\begin{titlepage}! and \verb!\end{titlepage}!, respectively.
Operating in this environment has several beneficial effects, e.g., disabling the page count.

In this script, however, we do not discuss in detail how to design a good title page.
Instead, it is recommended to look at the template that is attached as supplementary material in the Snippets folder.


\section{Acronyms}
\label{ch:basic.acronyms}

For easy handling of acronyms, we first have to load the package \textit{acronyms} \autocite{oetikerAcronymEnvironmentLATEX2020}.
Once this is done, we can define acronyms in the eponymous environment, which is enclosed by the tags \verb!\begin{acronym}! and \verb!\end{acronym}!.
In general, it is recommended to move such acronym definitions to a separate file, e.g., \texttt{acronyms.tex}.
Furthermore, to add a corresponding entry to the table of contents, the environment has to be preceded by the command \verb!\addchap{}!, where the entry name is to be passed as an argument.

To define an acronym inside the environment, we use the command \verb!\acro{}[]{}!.
Here, the first argument is basically a label\footnote{These labels are different from those used for sections, figures, etc., so it is not necessary to use a prefix \texttt{ac:} or something similar here (see \cref{ch:basic.references}).}, the second one is the acronym itself and the third one is its full form.
Then, outside the environment, we can refer to an acronym by the command \verb!\ac{}!, where we have to pass the acronym's label as an argument.
The first use of an acronym results in the full form followed by the acronym itself in braces, whereas any further use only prints the acronym.
If we want to force the long or short form, however, we can invoke the commands \verb!\acl{}! and \verb!\acs{}!, respectively.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \begin{acronym}
            \acro{dlr}[DLR]{German Aerospace Center}
        \end{acronym}

        \acs{dlr}, \ac{dlr}, \ac{dlr}, \acl{dlr}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \acs{dlr}, \ac{dlr}, \ac{dlr}, \acl{dlr}.
    \end{indentBuilt}
\end{example}
\vspace{1cm}

\noindent For more information and sophisticated functions, it is recommended to read the documentation \autocite{oetikerAcronymEnvironmentLATEX2020}.

A far more powerful package than acronyms is \textit{glossaries} \autocite{talbotUserManualGlossaries2020}. 
However, it is complex and requires special extra steps to make use of it, so it is recommended only for advanced users. 
Once it is useable, glossaries can automatically sort all acronyms and glossary entries. There are four different sorting methods available. 
It is recommended to use \textit{xindy} \autocite{kehrXindyManual2004} here,
which can be selected as a package option when including glossaries.


LatexMk is able to take care of the extra compilation steps. Look at the \texttt{.latexmkrc} file in the Snippets folder to get a rough idea. The file has to be placed in the project's root folder to be read automatically.\footnote{This file uses LuaTeX instead of pdfTeX. Change to LuaTeX or modify the file to use pdfTeX.}

\section{Cross-references}
\label{ch:basic.references}

When we write a long document with many interdependencies between individual sections, the use of cross-references is helpful to increase readability and facilitate understanding.
Besides, it is good style to refer to all figures, tables, listings, etc. in the text.

To create a cross-reference to one of the aforementioned objects, we first have to make it referenceable.
This is done by the command \verb!\label{}!, where we pass an identifier for the object as an argument.
It is useful to establish a naming convention for identifiers of different types of objects in order to not throw things together. \Cref{tab:labelNamingConvention} shows a recommendation for such a naming convention.

\begin{table}[!ht]
    \centering
    \caption[Recommended naming convention for labels]{Recommended naming convention for labels. The left column contains the object type, the right one the proposed naming convention. In general, the object type determines a prefix, which is separated from the actual object identifier by a colon. Object identifiers are written in camel case. Furthermore, a hierarchy (e.g., for sections or subfigures) is modeled by a baseline dot as a separator between identifiers of different hierarchy levels.}\label{tab:labelNamingConvention}

    \begin{tabular}{ll} 
        \toprule
        Object type & Naming convention\\
        \midrule
        Chapter & \texttt{ch:[\ldots]} \\
        Section & \texttt{ch:[\ldots].[\ldots]} \\
        Equation & \texttt{eq:[\ldots]} \\
        Figure & \texttt{fig:[\ldots]} \\
        Subfigure & \texttt{fig:[\ldots].[\ldots]} \\
        Listing & \texttt{lst:[\ldots]} \\
        Table & \texttt{tab:[\ldots]} \\
        \bottomrule
    \end{tabular}
\end{table}
\noindent After a label has been set, we can refer to the corresponding object by the command \verb!\ref{}!, which takes the identifier as an argument.


\subsection{Hyperref}
\label{ch:basic.references.hyperref}

The package \textit{hyperref} \autocite{rahtzHypertextMarksLaTeX2021} extends the cross-references by automatically setting hyperlinks in the document.
Therefore, readers can simply click on a cross-reference and jump directly to the original text passage or object.

We actually do not have to invoke any special command to make hyperref work, but just need to load the package initially.
However, regarding the order of package loading, hyperref is somewhat peculiar.
In any case, it has to be loaded after BibLaTeX, and it should generally be loaded as one of the last packages.
Two exceptions are geometry and the cleveref package presented below, which are recommended and required to be loaded after hyperref, respectively.

The default configuration of hyperref creates colorful bounding boxes at the cross-references.
To suppress this visual emphasis, we can pass the optional argument \texttt{hidelinks} when loading the package.


\subsection{Cleveref}
\label{ch:basic.references.cleveref}

Unfortunately, the aforementioned command \verb!\ref{}! provides only the index of a referenced object, so we have to manually prefix it with the name of the object type when creating a cross-reference.
For a large document, this can be annoying, although a good naming convention indicates a referenced object's type.

Here, the package \textit{cleveref} \autocite{cubittCleverefPackage2018} produces relief: 
It provides the command \verb!\cref{}!, which basically works like \verb!\ref{}!, but does the prefixing automatically.
If we start a new sentence with a cross-reference, the command \verb!\Cref{}! analogously capitalizes the first word.
In addition, when we need to refer to several objects all at once, we can simply pass their labels as a comma-separated sequence and cleveref creates a suitable formulation.
However, when doing so, we have to ensure that there is no space between a comma and the subsequent label.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \cref{tab:labelNamingConvention}\\
        \Cref{tab:labelNamingConvention}\\
        \Cref{tab:fontSizes,tab:labelNamingConvention}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \cref{tab:labelNamingConvention}\\
        \Cref{tab:labelNamingConvention}\\
        \Cref{tab:fontSizes,tab:labelNamingConvention}
    \end{indentBuilt}
\end{example}
\vspace{1cm}

Last, cleveref also provides additional commands to facilitate, e.g., the formulation of ranges.
These are explained in detail in the package's documentation \autocite{cubittCleverefPackage2018}.


\section{Bab(b)el}
\label{ch:basic.babel}

With the \textit{babel} package\footnote{Not to be confused with the \textit{Babbel} learning platform :D.} \autocite{braamsBabel2021}, we can set the default language of the document
and even switch between multiple languages. 
LTex (the language checker in VS Codium) follows the selected language.

To begin a new language environment, e.g., in German, we write:

\begin{indentVerbatimOnly}
    \begin{otherlanguage}{ngerman}
        Guten Tag!
    \end{otherlanguage}
\end{indentVerbatimOnly}

\noindent Alternatively, the language can be set for all following text by the command\\\verb!\selectlanguage{ngerman}!.
In any case, the babel subpackage that corresponds to the selected language has to be installed as well.


\section{BibLaTeX and Citations}
\label{ch:basic.biblatex}

When we write a scientific work, it is essential to cite other works, on which we base our thoughts.
The package \textit{BibLaTeX} \autocite{lehmanBiblatexPackage2020} (imported as \texttt{biblatex}) manages the citations for us.
It is recommended to use the \textit{biber} backend \autocite{kimeBiberBackendBibliography2020} here.
BibLaTeX also offers several citation styles, which we do not discuss in detail within the scope of this script.
In the scientific area, a common one is the numeric \ac{ieee} citing style \autocite{ieeeassociationIEEEREFERENCEGUIDE2018}, which is used both in the following and internally in this script.
When loading BibLaTeX, biber and the \ac{ieee} style can be applied as follows:

\begin{indentVerbatimOnly}
    \usepackage[backend=biber, sorting=none, style=numeric-comp]{biblatex}
\end{indentVerbatimOnly}
\vspace{.2cm}

\noindent Literature to be cited is then added to a \texttt{.bib} file in a \ac{json}-like format, acting as a database.
Every entry in this database needs is unique identifier.
Moreover, the file \texttt{.bib} needs to be included by the command \verb!\addbibresource{}! in the preamble of the \LaTeX~document, where the path to it is passed as an argument.

After an entry has been added to the \texttt{.bib} file, we can cite it by the command \verb!\autocite{}!, which automatically applies the chosen citing style and takes the identifier of the entry as an argument.
The commands \verb!\citeauthor{}! and \verb!\citeyear{}! work similarly, but print the authors and the publication year of the cited literature, respectively.
Both the details of the BibLaTeX syntax, including the available article types as well as their individual attributes, and further such commands that print other attributes can be found in the documentation of BibLaTeX \autocite{lehmanBiblatexPackage2020}.

\begin{example}
    The BibLaTeX entry

    \begin{indentVerbatim}
        @inreference{GermanAerospaceCenter2021,
            title = {German {{Aerospace Center}}},
            author = {Anonymous},
            booktitle = {Wikipedia},
            date = {2021-07-27},
            url = {https://en.wikipedia.org/wiki/German_Aerospace_Center},
        }
    \end{indentVerbatim}

    and the \LaTeX~code

    \begin{indentVerbatim}
        The Wikipedia page on the \ac{dlr} \autocite{anonymousGermanAerospaceCenter2021} was published by \citeauthor{anonymousGermanAerospaceCenter2021} in \citeyear{anonymousGermanAerospaceCenter2021}.
    \end{indentVerbatim}

    \noindent lead to

    \begin{indentBuilt}
        The Wikipedia\footnotemark~page on the \ac{dlr} \autocite{anonymousGermanAerospaceCenter2021} was published by \citeauthor{anonymousGermanAerospaceCenter2021} in \citeyear{anonymousGermanAerospaceCenter2021}.
    \end{indentBuilt}
\end{example}
\vspace{1cm}
\footnotetext{Note that citing Wikipedia is generally highly disrecommended :P.}

Last, we can print a list of cited literature by invoking the command \verb!\printbibliography!.
This is best done after the command \verb!\backmatter!.
To add the corresponding entry in the table of contents, we must also invoke

\begin{indentVerbatimOnly}
    \addcontentsline{toc}{chapter}{Bibliography}
\end{indentVerbatimOnly}

\noindent beforehand (but still after \verb!\backmatter!), where the last argument can be replaced by the desired entry shown in the table of contents.


\section{Footnotes}
\label{ch:basic.footnotes}

Eventually, to define a footnote, we simply invoke the command \verb!\footnote{}! and pass the footnote text as an argument. 