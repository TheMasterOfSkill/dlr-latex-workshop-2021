%ITEX root = document.tex
\chapter{Figures, Graphics and Plots}
\label{ch:figures}

In the following, it is explained how to include graphics in \LaTeX.
First, in \cref{ch:figures.images}, we discuss the basic package to define figures.
Subsequently, \cref{ch:figures.subfigures} shows how to place several subfigures next to each other, being part of a surrounding figure.
Last, \cref{ch:figures.tikz,ch:figures.plots} introduce packages to draw and plot vector graphics directly in \LaTeX.


\section{Including images}
\label{ch:figures.images}

To include images into a \LaTeX~document, we use the \textit{graphicx} package \autocite{carlislePackagesGraphicsBundle2021}.
Most often it is useful to define a single directory that holds all project
images. 
This can be done by the command \verb!\graphicspath{}!, where the path to the directory is passed as an argument.
It is highly recommended using a relative path here, since you might want to
compile the document on a different machine and sync the project via
Git\footnote{If you want to automatically build the document with GitLab see the
gitlab-ci.yml file in the Snippets directory}.

\begin{indentVerbatimOnly}
    \graphicspath{<path>}
\end{indentVerbatimOnly}

Then, images can be included by the command \verb!\includegraphics{}!, with the filename being passed as an argument.
Moreover, we can specify the width and the aspect ratio as further optional arguments. 
It is advisable to embed the command into a \textit{figure} environment, which internally uses the \texttt{float} environment.
That way \LaTeX~automatically moves the figure to the place where it fits best and allows the usage of captions.

However, in some cases, we want to define the position by ourselves. 
In such a case, the float environment can be suppressed by using optional arguments, e.g.,

\begin{indentVerbatimOnly}
    \begin{figure}[!ht]
\end{indentVerbatimOnly}

\noindent to place the figure at the current position.

\begin{figure}[H]
    \begin{example}
        The \LaTeX~code:

        \begin{indentVerbatimX2}
            \begin{figure}[H]
                \centering
                \includegraphics[width=.4\textwidth, keepaspectratio]{../img/dlr_logo.pdf}
                \caption{1 DLR, honestly}\label{fig:1DLR}
            \end{figure}
        \end{indentVerbatimX2}

        \noindent leads to

        \begin{indentBuilt}
            \begin{center}
                \includegraphics[width=.4\textwidth, keepaspectratio]{../img/dlr_logo.pdf}
            \end{center}
            \vspace{0.1cm}
            \exfigurecaption{1 DLR, honestly.}
            \label{fig:1DLR}
        \end{indentBuilt}
    \end{example}
\end{figure}

The \verb!\caption{}! command can also take an optional argument in square brackets. 
In that case, the optional argument is displayed as entry in the list of
figures, and the mandatory argument below the figure itself. 


\section{Subfigures}
\label{ch:figures.subfigures}

To embed multiple subfigures in a figure, we can use the package \textit{subcaption} \autocite{sommerfeldtSubcaptionPackage2020}. 
Then, within a \texttt{figure} environment, we do not invoke \verb!\includegraphics{}! directly, but invoke the command \verb!\subcaptionbox{}! for each subfigure to be created.
Here, the call of the corresponding \verb!\includegraphics{}! is passed as an argument.
Besides, we have to pass both the caption and the label of the subfigure as a further optional argument.
Finally, between several \verb!subcaptionbox{}! calls, we invoke the command \verb!\hspace{\fill}! to utilize the maximum space that is provided by the page.
In the end, we can simply define a caption and a label for the surrounding figure as before.

\begin{figure}[H]
    \begin{example}
        The \LaTeX~code

        \begin{indentVerbatimX2}
            \begin{figure}
                \subcaptionbox{DLR\label{fig:logos.dlr}} {\includegraphics[width=0.4\textwidth, keepaspectratio]{../img/dlr_logo}}
                \hspace{\fill}
                \subcaptionbox{DHBW\label{fig:logos.dhbw}} {\includegraphics[width=0.4\textwidth, keepaspectratio]{../img/dhbw_logo}}
                \caption{Logos of the \ac{dlr} and the \ac{dhbw}.}
                \label{fig:logos}
            \end{figure}
        \end{indentVerbatimX2}

        \noindent leads to

        \begin{indentBuilt}
            \begin{center}
                \subcaptionbox{DLR\label{fig:logos.dlr}}{\includegraphics[width=0.4\textwidth - 0.4\indentoffset, keepaspectratio]{../img/dlr_logo}}
                \hspace{\fill}
                \subcaptionbox{DHBW\label{fig:logos.dhbw}}{\includegraphics[width=0.4\textwidth - 0.4\indentoffset, keepaspectratio]{../img/dhbw_logo}}
            \end{center}
            \vspace{0.1cm}
            \exfigurecaption{Logos of the DLR and the DHBW.}
            \label{fig:logos}
        \end{indentBuilt}
    \end{example}
\end{figure}


\section{Creating Vector Graphics in \LaTeX}
\label{ch:figures.tikz}

\textit{Tikz} \autocite{tikzAndPgfManualTantau2021} is a powerful package to create vector graphics, coming with a bunch of sub-packages. 
While it is too large to be covered in this document, the documentation is well maintained, and there are many examples over the internet.\\
The syntax might look a bit overwhelming, but once you learned how to use nodes and set them relative to each other, this package will come in handy with all kind of plots and drawings. 

\begin{example}
    The \LaTeX~code
    \begin{indentVerbatim}
        \newcommand{\pythagwidth}{3cm}
        \newcommand{\pythagheight}{2cm}
        \begin{tikzpicture}
        \coordinate [label={below right:$A$}] (A) at (0, 0);
        \coordinate [label={above right:$B$}] (B) at (0, \pythagheight);
        \coordinate [label={below left:$C$}] (C) at (-\pythagwidth, 0);

        \coordinate (D1) at (-\pythagheight, \pythagheight + \pythagwidth);
        \coordinate (D2) at (-\pythagheight - \pythagwidth, \pythagwidth);

        \draw [very thick] (A) -- (C) -- (B) -- (A);

        \newcommand{\ranglesize}{0.3cm}
        \draw (A) -- ++ (0, \ranglesize) -- ++ (-\ranglesize, 0) -- ++ (0, -\ranglesize);

        \draw [dashed] (A) -- node [below] {$b$} ++ (-\pythagwidth, 0)
                    -- node [right] {$b$} ++ (0, -\pythagwidth)
                    -- node [above] {$b$} ++ (\pythagwidth, 0)
                    -- node [left]  {$b$} ++ (0, \pythagwidth);
        \end{tikzpicture}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \begin{center}
            \newcommand{\pythagwidth}{3cm}
            \newcommand{\pythagheight}{2cm}
            \begin{tikzpicture}
            \coordinate [label={below right:$A$}] (A) at (0, 0);
            \coordinate [label={above right:$B$}] (B) at (0, \pythagheight);
            \coordinate [label={below left:$C$}] (C) at (-\pythagwidth, 0);

            \coordinate (D1) at (-\pythagheight, \pythagheight + \pythagwidth);
            \coordinate (D2) at (-\pythagheight - \pythagwidth, \pythagwidth);

            \draw [very thick] (A) -- (C) -- (B) -- (A);

            \newcommand{\ranglesize}{0.3cm}
            \draw (A) -- ++ (0, \ranglesize) -- ++ (-\ranglesize, 0) -- ++ (0, -\ranglesize);

            \draw [dashed] (A) -- node [below] {$b$} ++ (-\pythagwidth, 0)
                        -- node [right] {$b$} ++ (0, -\pythagwidth)
                        -- node [above] {$b$} ++ (\pythagwidth, 0)
                        -- node [left]  {$b$} ++ (0, \pythagwidth);
            \end{tikzpicture}
        \end{center}
    \end{indentBuilt}
\end{example}

\section{Plotting in \LaTeX}
\label{ch:figures.plots}

\textit{PGFPlots} \autocite{tikzAndPgfManualTantau2021} is based on \textit{TikZ} and can create various plots. 
As for the latter, this package is far too large to be discussed in detail here, so we just go through the fundamentals.

PGFPlots is able to do many kinds of calculations and plot their results (e.g.,
intersection of graphs). Octave is used as the backend. Thus, it is also
possible to read data from files and visualize it afterwards with
PGFPlots\footnote{Look at the package \textit{pgfplotstable}
\autocite{dr.feuersangerManualPackagePGFPlotsTable2021} to handle tables with
PGFPlots}.

In the following, an example is given below \textit{IS-LM diagram} comment in line \num{10}.
PGFPlots resp. TikZ is told to fit a parabola between the points \textit{LM\_1} at $ (0.2,0.3) $ and \textit{LM\_2} at $(1.8,1.8)$. 
If the two functions weren't parabolas but e.g. straight lines, PGFPlots would be able to compute the intersection point by itself. 
Here, in line \num{18}, a node is placed manually at the right location.

The basic syntax to define a node is

\begin{indentVerbatimOnly}
    \node[<node options>] at (<location>) (<name>) {<Text inside node>};
\end{indentVerbatimOnly}

\noindent The node command -- or think more of it as an object -- can have many options and its syntax works as follows: 
The first section (square brackets) controls the appearance and attachments of the node. 
Right after the keyword \textit{at}, we can type in the location. 
It is also possible to reference other nodes by their name and set the location relative to theirs.
This can also be achieved by typing something like \textit{below= 3cm of <name>} into the node options.

Each node is identified by a name and can thus be called in other functions and
nodes. In consequence, the name must be unique in the scope of the environment.
If the node shape is larger (e.g., a textbox), we can get the location of its
corners, e.g., by \textit{<name>.south east}, which would be the lower right
corner of the node.

Last, we must never forget to terminate a TikZ command by typing \enquote{;}. 
Otherwise, strange errors can occur. 

\begin{example}
    \begin{indentVerbatim}
        \newcommand\LM{\ensuremath{\mathit{LM}}}
        \newcommand\IS{\ensuremath{\mathit{IS}}}
        \begin{tikzpicture}[
            scale=2,
            IS/.style={blue, thick},
            LM/.style={red, thick},
            ...,
        ]
        % axis
        \draw[axis,<->] (2.5,0) node(xline)[right] {$Y$} -|
                        (0,2.5) node(yline)[above] {$i$};
        % IS-LM diagram
        \draw[LM] (0.2,0.3) coordinate (LM_1) parabola (1.8,1.8)
            coordinate (LM_2) node[above] {\LM};
        \draw[IS] (0.2,1.8) coordinate (IS_1) parabola[bend at end]
            (1.8,.3) coordinate (IS_2) node[right] {\IS};
        %Intersection is calculated "manually" since Tikz does not offer intersection calculation for parabolas
        \node[dot,label=above:$A$] at (1,.68) (int1) {};
        \end{tikzpicture}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \newcommand\LM{\ensuremath{\mathit{LM}}}
        \newcommand\IS{\ensuremath{\mathit{IS}}}
    \begin{tikzpicture}[
        scale=2,
        IS/.style={blue, thick},
        LM/.style={red, thick},
        axis/.style={very thick, ->, >=stealth', line join=miter},
        important line/.style={thick}, dashed line/.style={dashed, thin},
        every node/.style={color=black},
        dot/.style={circle,fill=black,minimum size=4pt,inner sep=0pt,
            outer sep=-1pt},
    ]
    % axis
    \draw[axis,<->] (2.5,0) node(xline)[right] {$Y$} -|
                    (0,2.5) node(yline)[above] {$i$};
    % IS-LM diagram
    \draw[LM] (0.2,0.3) coordinate (LM_1) parabola (1.8,1.8)
        coordinate (LM_2) node[above] {\LM};
    \draw[IS] (0.2,1.8) coordinate (IS_1) parabola[bend at end]
         (1.8,.3) coordinate (IS_2) node[right] {\IS};
    %Intersection is calculated "manually" since Tikz does not offer
    %intersection calculation for parabolas
    \node[dot,label=above:$A$] at (1,.68) (int1) {};
\end{tikzpicture}
    \end{indentBuilt}
\end{example}

