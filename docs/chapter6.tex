\chapter{Tables}
\label{ch:tables}

In this chapter, the process of defining a table in \LaTeX~is described.
\Cref{ch:tables.tabular} shows the basic \LaTeX-internal commands to create a table.
Then, in \cref{ch:tables.tabularx}, the tabularx package is introduced, extending this basic functionality.
Finally, \cref{ch:tables.booktabs} lists some guidelines to define a visually appealing table.

\section{Tabular}
\label{ch:tables.tabular}

The basic functionality to create tables is already included in \LaTeX.
When defining a table, there are some similarities to the process of setting up a figure (see \cref{ch:figures.images}).

First, these tables reside also in a floating environment, which in this case is opened by \verb!\begin{table}! and closed by \verb!\end{table}!.
Second, just as a figure, a table is supposed to have a caption.
Opposite to a figure, however, it is convention to place the caption not below, but above the table.
Third, it is also mandatory to refer to each table at least once in the main text, so a table necessarily needs its label as well.

Where a figure is created by invoking the command \verb!\includegraphics{}!, a default \LaTeX~table is defined in another, nested environment that is opened and closed by \verb!\begin{tabular}! and \verb!\end{tabular}!, respectively.
The opening tag here takes another argument, which determines the column layout.
It is stated as a permutation of five symbols, which are explained in \cref{tab:defaultTableSymbols}.

\begin{table}[!ht]
    \centering
    \caption[Default table layout symbols]{Default table layout symbols. The left column contains the symbol, the right one corresponding explanation.}\label{tab:defaultTableSymbols}

    \begin{tabular}{ll} 
        \toprule
        Symbol & Explanation\\
        \midrule
        \texttt{l} & Left-justified column \\
        \texttt{c} & Centered column \\
        \texttt{r} & Right-justified column \\
        \verb!p{}! & Fixed-width paragraph column \\
        \texttt{|} & Vertical line (separator) \\
        \bottomrule
    \end{tabular}
\end{table}

\noindent Then, inside the \texttt{tabular} environment, the symbols \verb!&! and \verb!\\! act as column and separators, respectively.
Note here that tables are defined in row-major order.
Last, to create horizontal lines, we can use the command \verb!\hline!.

\begin{table}[!ht]
    \begin{example}
        The \LaTeX~code

        \begin{indentVerbatimX2}
            \begin{table}
                \caption{This is a tabular table with numbers and some text.}
                \label{tab:tabularTable}
                \centering
                \begin{tabular}{lcr|p{4cm}}
                    Number A & Number B & Number C & Text \\
                    \hline
                    1 & 2 & 3 & This is the first row, and it contains the numbers \num{1}, \num{2}, \num{3}. \\
                    4 & 5 & 6 & This is the second row, and it contains the numbers \num{4}, \num{5}, \num{6}. \\
                    7 & 8 & 9 & This is the third row, and it contains the numbers \num{7}, \num{8}, \num{9}. \\
                \end{tabular}
            \end{table}
        \end{indentVerbatimX2}

        \noindent leads to

        \begin{indentBuilt}
            \extablecaption{This is a tabular table with numbers and some text.}
            \label{tab:tabularTable}
            \begin{center}
                \begin{tabular}{lcr|p{4cm}}
                    Number A & Number B & Number C & Text \\
                    \hline
                    1 & 2 & 3 & This is the first row, and it contains the numbers \num{1}, \num{2}, \num{3}. \\
                    4 & 5 & 6 & This is the second row, and it contains the numbers \num{4}, \num{5}, \num{6}. \\
                    7 & 8 & 9 & This is the third row, and it contains the numbers \num{7}, \num{8}, \num{9}. \\
                \end{tabular}
            \end{center}
        \end{indentBuilt}
    \end{example}
\end{table}


\section{Tabularx}
\label{ch:tables.tabularx}

The \textit{tabularx} package \autocite{carlisleTabularxPackage2020} extends the regular \texttt{tabular} environment by the feature of setting fixed table widths and variable column widths.
To use the feature, we first have to replace the \texttt{tabular} environment by the \texttt{tabularx} environment.
Then, in addition to the layout symbols in \cref{tab:defaultTableSymbols}, we can use the symbol \texttt{X} for a left-justified column, whose width is the remaining \enquote{flexible} width of the table divided by the number of columns with variable width.
Moreover, we have to set the width of the table as a further, preceding argument of the \verb!\begin{tabularx}!.

\begin{table}[!ht]
    \begin{example}
        The \LaTeX~code

        \begin{indentVerbatimX2}
            \begin{table}
                \caption{This is a tabularx table with numbers and some text.}
                \label{tab:tabularxTable}
                \centering
                \begin{tabularx}{\textwidth}{lcr|X}
                    Number A & Number B & Number C & Text \\
                    \hline
                    1 & 2 & 3 & This is the first row, and it contains the numbers \num{1}, \num{2}, \num{3}. \\
                    4 & 5 & 6 & This is the second row, and it contains the numbers \num{4}, \num{5}, \num{6}. \\
                    7 & 8 & 9 & This is the third row, and it contains the numbers \num{7}, \num{8}, \num{9}. \\
                \end{tabularx}
            \end{table}
        \end{indentVerbatimX2}

        \noindent leads to

        \begin{indentBuilt}
            \extablecaption{This is a tabularx table with numbers and some text.}
            \label{tab:tabularxTable}
            \begin{center}
                \begin{tabularx}{\textwidth - \indentoffset}{lcr|X}
                    Number A & Number B & Number C & Text \\
                    \hline
                    1 & 2 & 3 & This is the first row, and it contains the numbers \num{1}, \num{2}, \num{3}. \\
                    4 & 5 & 6 & This is the second row, and it contains the numbers \num{4}, \num{5}, \num{6}. \\
                    7 & 8 & 9 & This is the third row, and it contains the numbers \num{7}, \num{8}, \num{9}. \\
                \end{tabularx}
            \end{center}
        \end{indentBuilt}
    \end{example}
\end{table}
\vspace{1cm}

\noindent For sophisticated possibilities of configuration, it is recommended to read the documentation of the package \autocite{carlisleTabularxPackage2020}.


\section{Booktabs}
\label{ch:tables.booktabs}

Last, the package \textit{booktabs} \autocite{fearPublicationQualityTables2020} defines some guidelines to create visually appealing tables.
Basically, the most important three guidelines are

\begin{enumerate}
    \item not to use vertical lines as column separators,
    \item not to use double lines either as horizontal or vertical separators,
    \item and not to put units in the table's body, but its heading.
\end{enumerate}

\begin{table}[!ht]
    \begin{example}
        The \LaTeX~code

        \begin{indentVerbatimX2}
            \begin{table}
                \caption{This is a tabularx + booktabs table with numbers and some text.}
                \label{tab:booktabsTable}
                \centering
                \begin{tabularx}{\textwidth}{lcrX}
                    Number A & Number B & Number C & Text \\
                    1 & 2 & 3 & This is the first row, and it contains the numbers \num{1}, \num{2}, \num{3}. \\
                    4 & 5 & 6 & This is the second row, and it contains the numbers \num{4}, \num{5}, \num{6}. \\
                    7 & 8 & 9 & This is the third row, and it contains the numbers \num{7}, \num{8}, \num{9}. \\
                \end{tabularx}
            \end{table}
        \end{indentVerbatimX2}

        \noindent leads to

        \begin{indentBuilt}
            \extablecaption{This is a tabularx + booktabs table with numbers and some text.}
            \label{tab:booktabsTable}
            \begin{center}
                \begin{tabularx}{\textwidth - \indentoffset}{lcrX}
                    \toprule
                    Number A & Number B & Number C & Text \\
                    \midrule
                    1 & 2 & 3 & This is the first row, and it contains the numbers \num{1}, \num{2}, \num{3}. \\
                    4 & 5 & 6 & This is the second row, and it contains the numbers \num{4}, \num{5}, \num{6}. \\
                    7 & 8 & 9 & This is the third row, and it contains the numbers \num{7}, \num{8}, \num{9}. \\
                    \bottomrule
                \end{tabularx}
            \end{center}
        \end{indentBuilt}
    \end{example}
\end{table}
\vspace{1cm}

\noindent For the horizontal lines that separate the table heading and the table body, the package also provides the commands \verb!\toprule!, \verb!\midrule! and \verb!\bottomrule!.
They can be used both in the regular \texttt{tabular} environment and the \texttt{tabularx} environment.