%!TEX root = ./document.tex
\chapter{Source Code Highlighting}
\label{ch:sourceCode}

This chapter gives an overview of possibilities to highlight source code.
First, \cref{ch:sourceCode.listings} presents the basic package listings.
Subsequently, in \cref{ch:sourceCode.minted}, we briefly introduce the more sophisticated package minted.

\section{Listings}
A simple package for highlighting source code is \textit{listings}
\autocite{hoffmannListingsPackage2020}. The rendering of the output is
completely handled by the \LaTeX~driver, so no further software is required.
Here, we can write source code within the environment \texttt{lstlisting}, which
can be customized by several options.

\begin{example}
    The \LaTeX~code

    \begin{indentVerbatim}
        \begin{lstlisting}[language=Python, frame=single, caption=Python example, label=lst:python-example]
            def a_function():
                print("Hello World!")
        \end{lstlisting}
    \end{indentVerbatim}

    \noindent leads to

    \begin{indentBuilt}
        \begin{lstlisting}[language=Python, frame=single, caption=Python example]
    def a_function():
        print("Hello World!")
        \end{lstlisting}
    \end{indentBuilt}
\end{example}
\vspace{1cm}

A more colorful output can be achieved by defining a custom style via
\textit{lstdefinestyle}. However, this requires listing all keywords and special
symbols, which is quite blunt work. An example style for python can be found in
\texttt{Snippets/lstlistings\_python.tex}. A lexer, by contrast, already has a
list of keywords and is better in coloring. Therefore, we won't elaborate
further on this topic and advise to read the next section. Nevertheless, if one
wants to use the listings package with coloring, it is recommended to search for
already created styles.

\section{Minted}
\label{ch:sourceCode.minted}

We recommend the far more powerful \textit{minted} package \autocite{mintedDocumentationPoore2017}, which uses the Python lexer \textit{Pygments} for highlighting. 
Pygments needs to be installed and included in the PATH variable. 
Moreover, LatexMk must be invoked with the \texttt{-{}-shell-escape} flag (already set in VS Code workspace settings file). 
However, to mitigate problems with building this script, we only present an example for minted code, but no truly rendered output.

First of all, we have to include the \textit{minted} package. 
Since we possibly want a list of listings, it is recommended to load the package with the following options.
The first option controls how minted counts the listings; here we select the chapter level, but section is possible as well. 
It is also recommended to use captions in combination with listings.
If we provide \texttt{newfloat}, the eponymous environment is used, providing a better integration of the caption package.  

\begin{indentVerbatimOnly}
    \usepackage[chapter, newfloat]{minted}
\end{indentVerbatimOnly}

Next, as with a style file of \textit{listings}, we can define the look of multiple coding languages and set various options to be used in every environment. 
Since there are many options available, it is useful to check out the documentation \autocite{mintedDocumentationPoore2017} in order to get an overview. 

The code snippet below defines a minted environment for \CPP~code.
The source code is then rendered between two lines (frame) with line numbering (lineos).
Furthermore, the environment breaks lines when they get too long, uses a tabsize of two spaces and eventually trims leading white spaces as well as tabs from the code by the option \texttt{autogobble}.
Minted is also able to print mathematical expressions inside our code, if we provide the escape symbols\footnote{Make sure to use a combination of escape symbols that doesn't interfere with the programming language} and set the mathescape option. 

With the two lower lines a new environment -- named \texttt{code} -- is defined and can be used from now on. 
The environment is set to the listing type, to be present in the list of listings. 
With the lower command we can control what should be displayed as a specifier in the caption. 

\begin{indentVerbatimOnly}
    \setminted[cpp]{frame=lines, linenos=true, breaklines,
        tabsize=2, autogobble, escapeinside=\%\%,mathescape=true}
    \newenvironment{code}{\captionsetup{type=listing}}{}
    \SetupFloatingEnvironment{listing}{name=Source Code}
\end{indentVerbatimOnly}

The following code generates a \CPP~example listing with the minted package.
The coloring style can be customized with the \textit{usemintedstyle}. 
There are various styles available, alternatively, we can also define a custom one.

\begin{figure}[H]   
    \begin{example}
        The \LaTeX~code

        \begin{indentVerbatim}
            \begin{code}
                \begin{minted}{cpp}
                    template < typename T > template < typename U > inline 
                    Array1D < T > Array1D < T >::operator*=( const U& scalar ){
                        T *tmpPtr;
                        for (tmpPtr = ptr; tmpPtr - ptr < N; ++tmpPtr) 
                            *tmpPtr *= scalar;
                        return *this;
                    }
                \end{minted}
                \caption{Old scalar multiplication operator of Array1D class}
                \label{lst:array1d_scalar_mult_old}
            \end{code} 
        \end{indentVerbatim}
        
        \noindent leads to

        \begin{indentBuilt}
            \begin{center}
                \includegraphics[width=.95\textwidth, keepaspectratio]{../img/minted_example.png}
            \end{center}
        \end{indentBuilt}
    \end{example}
\end{figure}