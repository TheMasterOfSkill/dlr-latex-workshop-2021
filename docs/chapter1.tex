%!TEX root = ./document.tex
\chapter{Introduction to \LaTeX}
\label{ch:introduction}

\LaTeX~is a typesetting software that is widely used in the scientific area.
It slightly differs from other known typesetting software like Microsoft Word that is based on the \ac{wysiwyg} principle, i.e., we type something and immediately see a preview of the printed result.

By contrast, \LaTeX~uses a compiling process, where we first typeset our document in the markup language \LaTeX~and then build/compile/render the print-ready \texttt{.tex} file(s) in a second step.
This kind of workflow is very similar to programming in compiled languages such as \CPP, so people who are familiar with it usually have little difficulty to adjust.
Only the syntax is sometimes a bit peculiar, which comes from the fact that the current version \LaTeXe~was released already in 1994.
For example, comments that are ignored in the building process are denoted by a preceding \verb!%!.
Moreover, structures like conditionals or loops are principally available, but their syntax is very cumbersome to common programming languages.

But opposite to \ac{wysiwyg}, \LaTeX~has the advantage that we are always totally capable of what we do.
This is particularly evident in the setting of mathematical formulas; anyone who ever worked with the graphical formula editor of Microsoft Word and, e.g., had to replace a single symbol in a large, complex formula can relate here \textellipsis press \enquote{F} to pay respects.

In the following, we go through the structure of a typical \LaTeX~document.
At the highest level of abstraction, it consists of the so-called \textit{preamble} and the \textit{document text}.
These are introduced in \cref{ch:introduction.preamble,ch:introduction.documentText}, respectively.
Finally, \cref{ch:introduction.frame} gives an example for a typical frame of a \LaTeX~document, which can be used as a basis for a paper or a thesis.


\section{The Preamble}
\label{ch:introduction.preamble}

In the preamble, we make all the preparations that are needed to write the actual document text.
It can be further structured in three parts:

\begin{enumerate}
    \item \textbf{Class definition:} Here, we choose one of several predefined layout templates, determining both the appearance of the printed document and the available \LaTeX~commands. This is covered by \cref{ch:introduction.preamble.classes}.
    \item \textbf{Package loading:} Subsequently, we can load modular packages that extend the feature set of \LaTeX. \Cref{ch:introduction.preamble.packages} deals with this part of the preamble.
    \item \textbf{Configurations and definitions:} Last, we can configure loaded packages or define own functions or environments. Since the latter can be quite complex, it is not discussed in this script. 
\end{enumerate}

\subsection{Classes}
\label{ch:introduction.preamble.classes}

The very first command in any \LaTeX~document is always \verb!\documentclass{}!, where we choose the so-called \textit{document class}, defining the type of the document we write.
The name of the chosen class is then passed as an argument between the curly brackets.
\LaTeX~provides a large number of predefined classes, a subset of the most important of which is listed in \cref{tab:classes}.

\begin{table}[!ht]
    \centering
    \caption[\LaTeX~document classes]{\LaTeX~document classes. The left column shows the name of the class, the right one a description of it.}\label{tab:classes}

    \begin{tabular}{lp{10cm}}
        \toprule
        Class name & Description\\
        \midrule
        \texttt{article} & For short articles; does neither provide the structuring level \texttt{chapter}, nor the commands \verb!\frontmatter!, \verb!\mainmatter! and \verb!\backmatter!.\\
        \texttt{report} & For longer articles and reports; does not provide the structuring commands \verb!\frontmatter!, \verb!\mainmatter! and \verb!\backmatter!.\\
        \texttt{book} & Book-like layout, per default two-sided.\\
        \texttt{beamer} & For presentations; since they are not subject of this workshop, it is recommended to read \url{https://en.wikibooks.org/wiki/LaTeX/Presentations} if needed.\\
        \bottomrule
    \end{tabular}
\end{table}

For some default classes -- namely, \texttt{article}, \texttt{report}, and \texttt{book} of the here shown --, there are also equivalents provided by the package \textit{KOMA-Script} \autocite{kohmKOMAScript2021}.
These are named \texttt{scrartcl}, \texttt{scrreprt} and \texttt{scrbook} and have some benefits over the default classes such as additional features.
Although these benefits are not discussed here in detail, they are reason enough to prefer the KOMA-Script classes if possible. 
A little peculiarity is that the German documentation of KOMA-Script is actually better maintained than the English one.
This is due to the fact that its main developer is German.

Since it is the class that provides both the structuring level \texttt{chapter} and the commands \verb!\frontmatter!, \verb!\mainmatter! and \verb!\backmatter!, \verb!scrbook! should be preferred when we write longer documents.
The purpose of those properties is explained in detail in \cref{ch:introduction.documentText}.

Last, we also have to set the paper size and the default font size when defining the document class.
While the use of A4 paper is obligatory in the most cases, it is recommended to use 12pt here.
This information is to be added as optional arguments in square brackets.
Therefore, the first line looks like

\begin{indentVerbatimOnly}
    \documentclass[12pt, a4paper]{scrbook}.
\end{indentVerbatimOnly}

\noindent If, however, a one-sided layout is desired, we simply add the optional argument \texttt{oneside}.


\subsection{Packages}
\label{ch:introduction.preamble.packages}

After this first line, we can load further \LaTeX~packages to extend the functionality.
This is done by the command \verb!usepackage{}!, which takes the name of the package as an argument.

While the majority of useful basic packages is presented in \cref{ch:basic}, this section shows three packages that are so important and should never be missing that they can actually be counted as part of the frame of any \LaTeX~document. 
These packages are \textit{inputenc} \autocite{jeffreyInputenc2021}, \textit{fontenc}, and \textit{microtype} \autocite{schlichtMicrotypePackage2021}.

First, inputenc allows writing umlauts and some other characters that exceed the \ac{ascii}.
However, using the default pdfTeX engine\footnote{The LuaTeX engine allows this as well. But as it is difficult to set up, we do not discuss it in this script.}, we can still not write any unicode character directly in a \texttt{.tex} file; the most of them can only be rendered by invoking special \LaTeX~commands.

Second, fontenc changes the character encoding to support European languages, enabling correct hyphenation and making the built documents searchable.

And third, microtype optimizes the spacing between words, among other things. 
In consequence, more words fit on the same piece of paper and the text looks \enquote{cleaner}.

To use them correctly, the following lines have to be added after the definition of the document class:

\begin{indentVerbatimOnly}
    \usepackage[utf8]{inputenc}
    \usepackage[T1]{fontenc}
    \usepackage{microtype}
\end{indentVerbatimOnly}


\section{Structuring the Document Text}
\label{ch:introduction.documentText}

Following the preamble, the actual document text is written in an environment that is opened and closed by the tags \verb!\begin{document}! and \verb!\end{document}!, respectively.
To increase the clarity of our \LaTeX~code, it is recommended not to write the whole document text in the root \texttt{.tex} file, but to create a separate file for each chapter, representing the top-level structuring level\footnote{Actually, there is also the structuring level \texttt{part}, which is even higher-level than \texttt{chapter}. It is, however, rather rarely used and not discussed in detail in the scope of this script.} of our document.
Implementing this modularity, a separate \texttt{.tex} file can be included in the document text by the command \verb!\include{}!.
Here, the relative path to the file is passed as an argument.

Besides the structuring level \texttt{chapter}, there are also \texttt{section}, \texttt{subsection}, \texttt{paragraph} and others that are not discussed in detail here, where the hierarchy follows that order.
Since it is bad style to use more than three numbered structuring levels, only \texttt{chapter}, \texttt{section} and \texttt{subsection} are numbered.
To start, e.g., a new chapter, we simply write \verb!\chapter{}!, where the name of the chapter is passed as an argument.
The same applies to the other structuring levels.

Last, to create the table of contents, we just need to invoke the command \\\verb!\tableofcontents!, reasonably preceding the first chapter.
Here, it is good style to count such pages that precede the main content by Roman numerals, opposite to the main content and post-content pages, which are counted by Arabic numerals.
This is automatically done by the commands \verb!\frontmatter!, \verb!\mainmatter! and \verb!\backmatter!, which are to be invoked right before the preceding part, the main content part and the post-content part start, respectively.

\clearpage
\section{Typical Frame of a \LaTeX~Document}
\label{ch:introduction.frame}

In the following, a typical frame of a \LaTeX~document is presented.
It is based on the \texttt{scrbook} class.

\begin{indentVerbatimOnly}
    % Preamble
    \documentclass[12pt, a4paper]{scrbook}

    \usepackage[utf8]{inputenc}
    \usepackage[T1]{fontenc}
    \usepackage{microtype}
    % Load further packages

    % Configure packages and make definitions

    % Document text
    \begin{document}
        \selectlanguage{english}
        \frontmatter

        \tableofcontents
        % other tables, e.g. of figures, tables, ...

        \mainmatter
        
        % Include chapters
        
        \backmatter

        % Add bibliography, glossar, appendix, ...
    \end{document}
\end{indentVerbatimOnly}
\vspace*{\fill}