# Installation

- DLR Software wird installiert über zentralen Softwarekorb
- Miktex muss im User-Mode installiert werden
- VS Codium Extensions laden
- Workshop Repo (^this) klonen inkl. VS Codium Workspace Settings JSON


# VS Codium Extensions
-	Bracket Pair Colorizer 2 (selbsterklärend)
-	Footsteps (Game Changer in großen Dokumenten, markiert und erlaubt dir das Springen zwischen den zuletzt editierten Zeilen)
-	LaTeX Language Support (viele LaTeX Snippets)
-	LTeX (Language Server)
-	Material Icon Theme (zu Ordnernamen wie „img“, „tex“, „lib“ und darin enthaltene Dateien passende Icons)
-	Path Autocomplete (selbsterklärend, super angenehm wenn man z.B. Grafiken einfügt)
-	PDF Preview (weil TexLab scheinbar keinen eigenen Viewer mitbringt; muss aus dem MS Marketplace heruntergeladen werden und hat leider nicht das SyncTeX Feature, zumindest habe ich es bisher nicht zum Laufen gebracht)
- ReWrap (automatische Zeilenumbrüche)
-	TexLab (Ersatz für LaTeX Workshop, nutzt intern das Language Server Protocol von Microsoft und ist damit ziemlich leichtgewichtig. Hat aber manche Features von LaTeX Workshop nicht)
-	Todo Tree (sehr nützlich für Annotations wie TODOs etc.)


# LaTeX Modules

- Komaklassen (scrreport inkl. appendix env, scrarticle)
- inputenc
- fontenc
- microtype
- libertine/lmodern


- acronyms (Dennis)
- babel (Dennis)
- biblatex (Fabrice)
- caption
- chngcntr
- geometry (Fabrice)
- hyperref (Fabrice)
- cleveref (Fabrice)
- xcolor (inkl. fett/kursiv; Fabrice)

- amsmath (Fabrice)
- siuntix (Fabrice)

- lstlisting (Dennis)
- minted (+)

- graphicx (Dennis)
- tikz (Dennis)
- pgfplots (Dennis)
- pgfplots-table (Dennis)
- subcaption (Fabrice)

- booktabs (Fabrice)
